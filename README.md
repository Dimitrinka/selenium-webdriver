# Selenium WebDriver



## Selenium WebDriver | Homework
1. Automate a test case that searches with Bing:

Navigate to Bing
Search for 'Telerik Academy Alpha' 
Validate the title of the first result as 'IT Career Start in 6 Months - Telerik Academy Alpha'

2. Automate a test case that searches with Google Chrome:

Navigate to Google Chrome
Accept or reject cookies
Search for 'Telerik Academy Alpha' 
Validate the title of the first result as 'IT Career Start in 6 Months - Telerik Academy Alpha'