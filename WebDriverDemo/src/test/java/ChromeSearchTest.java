import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.ArrayList;

public class ChromeSearchTest {
    private static WebDriver driver;

    @Before
    public void startBrowser() {
        System.setProperty("webdriver.chrome.driver", "C:\\Didi\\BrowserDrivers\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @Test
    public void areSearchedTitleIsCorrect_ValidateTheTitle_IsCorrect(){

        driver.navigate().to("https://www.google.com/");
        driver.findElement(By.id("L2AGLb")).click();
        driver.findElement(By.xpath("/html/body/div[1]/div[3]/form/div[1]/div[1]/div[1]/div/div[2]/input")).sendKeys("Telerik Academy Alpha");
        driver.findElement(By.xpath("/html/body/div[1]/div[3]/form/div[1]/div[1]/div[1]/div/div[2]/input")).sendKeys(Keys.ENTER);

        ArrayList<String> browserTabs = new ArrayList<>(driver.getWindowHandles());
        driver.switchTo().window(browserTabs.get(0));

        WebElement firstTitle = driver.findElement(By.xpath("//h3[contains (text(),'IT Career Start in 6 Months - Telerik Academy Alpha')]"));
        Assert.assertEquals("Title is not the same!","IT Career Start in 6 Months - Telerik Academy Alpha",firstTitle.getText());

    }
    @After
    public void closeSession() {
        driver.quit();
    }

}
