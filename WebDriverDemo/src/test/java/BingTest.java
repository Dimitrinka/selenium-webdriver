import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;


import java.util.ArrayList;

public class BingTest {

    private static WebDriver driver;
    @Before
    public void startBrowser(){
        System.setProperty("webdriver.chrome.driver", "C:\\Didi\\BrowserDrivers\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }
    @Test
    public void isDisplayedSearchInput_NavigateToBingSearchInput_IsDisplayed(){
        driver.navigate().to("https://www.bing.com/");
        WebElement inputSearch = driver.findElement(By.id("sb_form_q"));
        Assert.assertTrue("Search input is not displayed.",inputSearch.isDisplayed());
    }
    @Test
    public void isEnabledSearchInput_NavigateToBingSearchInput_IsEnabled(){
        driver.navigate().to("https://www.bing.com/");

        WebElement inputSearch = driver.findElement(By.id("sb_form_q"));
        Assert.assertTrue("Search input is not enabled.",inputSearch.isEnabled());
    }
    @Test
    public void areSearchedTitleIsCorrect_ValidateTheTitle_IsCorrect(){

        driver.navigate().to("https://www.bing.com/");
        driver.findElement(By.xpath("//*[@id=\"sb_form_q\"]")).sendKeys("Telerik Academy Alpha");
        driver.findElement(By.id("search_icon")).click();

        ArrayList<String> browserTabs = new ArrayList<>(driver.getWindowHandles());
        driver.switchTo().window(browserTabs.get(0));

        WebElement firstTitle = driver.findElement(By.xpath("//a[contains(text(),'IT Career Start in 6 Months - Telerik Academy Alpha')]"));
        Assert.assertEquals("Title is not the same!","IT Career Start in 6 Months - Telerik Academy Alpha",firstTitle.getText());

    }
    @After
    public void closeSession() {
         driver.quit();
    }
}
